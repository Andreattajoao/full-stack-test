import jwt, { Secret, JwtPayload } from 'jsonwebtoken';
import e, { Request, Response, NextFunction } from 'express';

export const SECRET_KEY: Secret = 'watch.tv.br';

export interface CustomRequest extends Request {
    token: string | JwtPayload;
}

export const auth = async (req: Request, res: Response, next: NextFunction) => {
    try {
    const token = req.header('Authorization')?.replace('Bearer ', '');
        
    if (!token) {
        throw new Error();
    }

    const decoded = jwt.verify(token, SECRET_KEY);
    (req as CustomRequest).token = decoded;

    next();
    
    } catch (error) {
        res.status(401).json({error : "authentication is necessary"});
    }
};