import { Sequelize } from 'sequelize';
import { User } from '../models/User';
import { Movie } from '../models/Movie';

export class Database {
  private static instance: Sequelize;
  
  private constructor() {}
  
  static getInstance(): Sequelize {
    if (!Database.instance) {
      Database.instance = new Sequelize({
        dialect: 'sqlite',
        storage: './dev.sqlite3'
      });

      User.initialize(Database.instance);
      Movie.initialize(Database.instance);

      Database.instance.sync();
    }
    return Database.instance;
  }
}