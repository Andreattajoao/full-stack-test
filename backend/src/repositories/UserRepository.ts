import { User } from "../models/User";

export class UserRepository {
  static async getAllUsers(): Promise<User[]> {
    return await User.findAll();
  }

  static async getUserById(id: number): Promise<User | null> {
    return await User.findByPk(id);
  }

  static async createUser(userData: Partial<User>): Promise<User> {
    return await User.create(userData);
  }

  static async updateUser(id: number, userData: Partial<User>): Promise<void> {
    await User.update(userData, { where: { id } });
  }

  static async deleteUser(id: number): Promise<void> {
    await User.destroy({ where: { id } });
  }
}