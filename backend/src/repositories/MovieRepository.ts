import { Movie } from '../models/Movie';

export class MovieRepository {
  static async getAllMovies(): Promise<Movie[]> {
    return await Movie.findAll();
  }

  static async getMovieById(id: number): Promise<Movie | null> {
    return await Movie.findByPk(id);
  }

  static async createMovie(movieData: Partial<Movie>): Promise<Movie> {
    return await Movie.create(movieData);
  }

  static async updateMovie(id: number, movieData: Partial<Movie>): Promise<void> {
    await Movie.update(movieData, { where: { id } });
  }

  static async deleteMovie(id: number): Promise<void> {
    await Movie.destroy({ where: { id } });
  }
}
