import express, { Express, Request, Response } from "express";
import routes from "./routes/routes";
import { Database } from "./db/Database";
import { auth } from "./middleware/auth.middleware";

const app = express();
const port = 3000;

const sequelize = Database.getInstance();

app.use(express.json());
app.use(routes);
app.use(auth);

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});