import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { User } from '../models/User';

export class AuthController {
  async login(req: Request, res: Response): Promise<void> {
    const { email, password } = req.body;

    try {
      const user = await User.findOne({ where: { email, password } });
      if (!user) {
        res.status(401).json({ error: 'Invalid credentials. Try again' });
        return;
      }

      const token = jwt.sign({ userId: user.id }, 'watch.tv.br', { expiresIn: '1h' });

      res.json({ token });
    } catch (error) {
      res.status(500).json({ error: 'Error in the login. Try again later.' });
    }
  }
}
