import { Request, Response } from 'express';
import { MovieService } from '../services/MovieService';
import { Movie } from '../models/Movie';

export class MovieController {
  async getAllMovies(req: Request, res: Response): Promise<void> {
    try {
      const movies: Movie[] = await MovieService.getAllMovies();
      res.json(movies);
    } catch (error) {
      res.status(500).json({ error: 'Something wrong happened. Try again' });
    }
  }

  async getMovieById(req: Request, res: Response): Promise<void> {
    const movieId = parseInt(req.params.id);
    try {
      const movie: Movie | null = await MovieService.getMovieById(movieId);
      if (movie) {
        res.json(movie);
      } else {
        res.status(401).json({ error: 'Movie not found. Try a different one' });
      }
    } catch (error) {
      res.status(500).json({ error: 'Something wrong happened. Try again' });
    }
  }

  async createMovie(req: Request, res: Response): Promise<void> {
    const movieData: Partial<Movie> = req.body;
    try {
      const newMovie: Movie = await MovieService.createMovie(movieData);
      res.status(201).json(newMovie);
    } catch (error) {
      res.status(500).json({ error: 'Something wrong happened. Try again' });
    }
  }

  async updateMovie(req: Request, res: Response): Promise<void> {
    const movieId = parseInt(req.params.id);
    const movieData: Partial<Movie> = req.body;
    try {
      const movie: Movie | null = await MovieService.getMovieById(movieId);
      if (!movie) {
        res.status(401).json({ error: 'Movie not found. Try a different one' });
        return;
      }
      await MovieService.updateMovie(movieId, movieData);
      res.json({ message: 'Movie updated with success!' });
    } catch (error) {
      res.status(500).json({ error: 'Something wrong happened, try again.' });
    }
  }

  async deleteMovie(req: Request, res: Response): Promise<void> {
    const movieId = parseInt(req.params.id);
    try {
      const movie: Movie | null = await MovieService.getMovieById(movieId);
      if (!movie) {
        res.status(401).json({ error: 'Movie not found. Try a different one' });
        return;
      }
      await MovieService.deleteMovie(movieId);
      res.json({ message: 'Movie deleted with success!' });
    } catch (error) {
      res.status(500).json({ error: 'Something wrong happened. Try again' });
    }
  }
}
