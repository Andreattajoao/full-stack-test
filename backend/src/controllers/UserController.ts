import { Request, Response } from 'express';
import { UserService } from '../services/UserService';
import { CreateUserRequest } from '../types/userRequest';
import { User } from '../models/User';

export class UserController {
    private userService: UserService;

    constructor() {
        this.userService = new UserService();
    }

    async getAllUsers(req: Request, res: Response): Promise<void> {
        try {
            const users = await this.userService.getAllUsers();
            res.json(users);
        } catch (error) {
            res.status(500).json({ error: 'Something wrong happened. Try again' });
        }
    }

    async getUserById(req: Request, res: Response): Promise<void> {
        const userId = parseInt(req.params.id);
        try {
            const user = await this.userService.getUserById(userId);
            if (user) {
                res.json(user);
            } else {
                res.status(401).json({ error: 'User not found. Try a different one' });
            }
        } catch (error) {
            res.status(500).json({ error: 'Something wrong happened. Try again' });
        }
    }

    async createUser(req: Request, res: Response): Promise<void> {
        const userData: CreateUserRequest = req.body;
        try {
            const newUser = await this.userService.createUser(userData);
            res.status(201).json(newUser);
        } catch (error) {
            res.status(500).json({ error: 'Something wrong happened, try again'});
        }
    }

    async updateUser(req: Request, res: Response): Promise<void> {
        const userId = parseInt(req.params.id);
        const userData: Partial<User> = req.body;
        try {
            const user: User | null = await this.userService.getUserById(userId);
            if (!user) {
                res.status(401).json({ error: 'User not found.' });
                return;
            }
            await this.userService.updateUser(userId, userData);
            res.json({ message: 'User updated with success!' });
        } catch (error) {
            res.status(500).json({ error: 'Something wrong happened. Try again' });
        }
    }

    async deleteUser(req: Request, res: Response): Promise<void> {
        const userId = parseInt(req.params.id);
        try {
            const user: User | null = await this.userService.getUserById(userId);
            if (!user) {
                res.status(401).json({ error: 'User not found.' });
                return;
            }
            await this.userService.deleteUser(userId);
            res.json({ message: 'User deleted with success!' });
        } catch (error) {
            res.status(500).json({ error: 'Something wrong happened. Try again' });
        }
    }
}
