export interface CreateUserRequest {
    fullName: string;
    email: string;
    password: string;
}