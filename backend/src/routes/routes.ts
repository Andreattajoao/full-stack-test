import { Router } from 'express';
import { UserController } from '../controllers/UserController';
import { MovieController } from '../controllers/MovieController';
import { AuthController } from '../controllers/AuthController';
import { auth } from '../middleware/auth.middleware';

const router = Router();

const userController = new UserController();
const movieController = new MovieController();
const authController = new AuthController();

router.post('/auth/login', authController.login.bind(authController));

router.post('/users/create', userController.createUser.bind(userController));
router.get('/users/get/:id', auth, userController.getUserById.bind(userController));
router.get('/users/getAll', auth, userController.getAllUsers.bind(userController));
router.post('/users/edit/:id', auth, userController.updateUser.bind(userController));
router.delete('/users/delete/:id', auth, userController.deleteUser.bind(userController));

router.get('/movies/get', auth, movieController.getAllMovies.bind(movieController));
router.get('/movies/get/:id', auth, movieController.getMovieById.bind(movieController));
router.post('/movies/create', auth, movieController.createMovie.bind(movieController));
router.post('/movies/edit/:id', auth, movieController.updateMovie.bind(movieController));
router.delete('/movies/delete/:id', auth, movieController.deleteMovie.bind(movieController));

export default router;
