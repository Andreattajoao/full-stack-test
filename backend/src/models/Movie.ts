import { Model, DataTypes, Sequelize } from 'sequelize';

export class Movie extends Model {
  static initialize(sequelize: Sequelize): void {
    this.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
          type: DataTypes.STRING,
          allowNull: false
        },
        year: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        description: {
          type: DataTypes.STRING,
          allowNull: false
        },
        bannerPath: {
            type: DataTypes.STRING,
            allowNull: false
          }
      },
      {
        sequelize,
        modelName: 'Movie'
      }
    );
  }
}
