import { MovieRepository } from '../repositories/MovieRepository';
import { Movie } from '../models/Movie';

export class MovieService {
  static async getAllMovies(): Promise<Movie[]> {
    try {
      return await MovieRepository.getAllMovies();
    } catch (error) {
      throw new Error('Erro ao buscar filmes');
    }
  }

  static async getMovieById(movieId: number): Promise<Movie | null> {
    try {
      return await MovieRepository.getMovieById(movieId);
    } catch (error) {
      throw new Error('Erro ao buscar filme por ID');
    }
  }

  static async createMovie(movieData: Partial<Movie>): Promise<Movie> {
    try {
      return await MovieRepository.createMovie(movieData);
    } catch (error) {
      throw new Error('Erro ao criar filme');
    }
  }

  static async updateMovie(movieId: number, movieData: Partial<Movie>): Promise<void> {
    try {
      await MovieRepository.updateMovie(movieId, movieData);
    } catch (error) {
      throw new Error('Erro ao atualizar filme');
    }
  }

  static async deleteMovie(movieId: number): Promise<void> {
    try {
      await MovieRepository.deleteMovie(movieId);
    } catch (error) {
      throw new Error('Erro ao excluir filme');
    }
  }
}
