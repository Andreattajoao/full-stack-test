import { UserRepository } from '../repositories/UserRepository';
import { User } from '../models/User';
import { CreateUserRequest } from '../types/userRequest';

export class UserService {
    async getAllUsers(): Promise<User[]> {
        try {
            const users = await UserRepository.getAllUsers();
            return users;
        } catch (error) {
            throw new Error('Error fetching users');
        }
    }

    async getUserById(userId: number): Promise<User | null> {
        try {
            const user = await UserRepository.getUserById(userId);
            return user;
        } catch (error) {
            throw new Error('Error fetching user');
        }
    }

  async createUser(userData: CreateUserRequest): Promise<User> {
    try {
        const existingUser = await User.findOne({ where: { email: userData.email } });
        if (existingUser) {
            throw new Error('This email is unavailable at the moment. Try again');
        }

        const newUser = await User.create(userData);
        return newUser;

        } catch (error) {
            throw new Error('Error creating the user');
        }
    }

    async updateUser(userId: number, userData: Partial<User>): Promise<void> {
        try {
            await UserRepository.updateUser(userId, userData);
        } catch (error) {
            throw new Error('Error updating the user');
        }
    }

    async deleteUser(userId: number): Promise<void> {
        try {
            await UserRepository.deleteUser(userId);
        } catch (error) {
            throw new Error('Error deleting the user');
        }
    }
}
