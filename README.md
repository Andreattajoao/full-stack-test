## Como buildar a aplicação e montar os ambientes

## Backend
    O backend foi desenvolvido em NodeJs, Typescript, Sequelize e sqlite3.

    1 - Abrir um terminal no diretório backend
    2 - Rodar o comando npm install para instalar todas as dependências
    3 - Rodar npx nodemon src/app.ts para inicializar a aplicação
    4 - Os endpoints podem ser acessados pela URL: http://localhost:3000

    Guia de API

    POST - /users/create 
    # Cria o usuario no database
    Body - {"fullName" : fullName, "email" : email, "password" : password}

    POST - /auth/login
    # Retorna o Bearer token para autenticar chamadas
    Body - {"email" : email, "password" : "password" }

    GET - /users/get/:id (Precisa de autenticação)
    # Retorna o usuário desejado

    GET - /users/getAll (Precisa de autenticação)
    # Retorna uma lista de todos os usuários

    POST - /users/edit/:id (Precisa de autenticação)
    Body - {"fullName" : fullName, "password" : "password" }
    # Edita o usuário desejado

    DELETE - /users/delete/:id (Precisa de autenticação)
    # Deleta o usuário desejado

    POST - /movies/create (Precisa de autenticação)
    Body: {"title" : title, "year" : year, "description" : description, "bannerPath" : banner Path}
    # Cria o filme no db

    GET - /movies/get/:id (Precisa de autenticação)
    # Retorna o filme desejado

    GET - /movies/getAll (Precisa de autenticação)
    # Retorna uma lista de todos os filmes

    POST - /movies/edit/:id (Precisa de autenticação)
    Body - {"title" : title, "year" : year, "description" : description, "bannerPath" : banner Path}
    # Edita o filme desejado

    DELETE - /movies/delete/:id (Precisa de autenticação)
    # Deleta o filme desejado

## Frontend
    O frontend foi desenvolvido em ReactJs utilizando a API do IMDB como a API externa solicitada

    1 - Abrir um terminal no diretório frontend
    2 - Roda o comando npm install para instalar as dependencias
    3 - Rodar npm run dev para inicializar a aplicação
    4 - A aplicação estará aberta na URL: http://localhost:5173/
    5 - Para realizar o login, utilize o email: joao.andreatta@watch.tv.br com a senha senhaSecreta123
