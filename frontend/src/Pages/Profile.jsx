import React from "react";

const Profile = () => {
  return (
    <div>
      <h2>Perfil do Usuário</h2>
      <p>Nome: João Victor</p>
      <p>Idade: 24</p>
      <p>Cidade: Curitiba</p>
      <p>Filme Preferido: Monstros S.A</p>
      <p>Email: joao.martineliandreatta@gmail.com</p>
    </div>
  );
};

export default Profile;
