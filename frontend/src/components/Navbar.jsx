import { Link, useNavigate } from "react-router-dom"

import { useState } from "react"

import { BiCameraHome, BiCameraMovie, BiSearchAlt2 } from 'react-icons/bi'

import './Navbar.css'

const Navbar = () => {


  return (
    <nav id="navbar">
        <h2>
          <Link to="/home"> <BiCameraHome /> Watch Brasil</Link>
        </h2>
        <h2>
          <Link to="/profile">Perfil</Link>
        </h2>
      </nav>
  )
}

export default Navbar