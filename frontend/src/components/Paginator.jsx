import React from "react";
import './Paginator.css'; // Importa o arquivo de estilos CSS

const Paginator = ({ page, totalPages, onNextPage, onPrevPage }) => {
  return (
    <div className="paginator">
      <button onClick={onPrevPage} disabled={page === 1}>
        Página Anterior
      </button>
      <span>Página {page} de {totalPages}</span>
      <button onClick={onNextPage} disabled={page === totalPages}>
        Próxima Página
      </button>
    </div>
  );
};

export default Paginator;
